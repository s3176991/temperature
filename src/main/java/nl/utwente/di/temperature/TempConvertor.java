package nl.utwente.di.temperature;

import java.util.HashMap;

public class TempConvertor {
    public TempConvertor() { }

    public double CtoF(String isbn) {
        double celsius = Double.parseDouble(isbn);

        return (celsius * (double) 9/5 ) + 32;
    }
}
